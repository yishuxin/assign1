var noon = 12;
function startTime(){
    var time = new Date();
    var min = time.getMinutes();
    var sec = time.getSeconds();
    var hr = time.getHours();
    var amPm = "AM";
    if (hr >= noon){
        amPm = "PM"
    } 
    if (hr > noon){
        hr = hr - noon;
    }
    min = checkTime(min);
    sec = checkTime(sec);
    document.querySelector("#currentTime").innerHTML= hr+ ":" + min + ":" + sec + " " + amPm;
    var t = setTimeout(startTime, 500);
}

function checkTime(i){
    if (i < 10){
        i = "0" + i;
    }
    return i;
}

startTime();



var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("slidesFade");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}