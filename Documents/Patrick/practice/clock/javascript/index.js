var menuToggle = document.querySelector("#menu-toggle");
var activeElements = document.querySelectorAll(".active-element");
var toggledMenu = menuToggle.addEventListener("click", function(){
     // forEach is not supported in IE11
  // activeElements.forEach(function(e){
  //     e.classList.toggle("active");
  // });
     for(var activated = 0; activated < activeElements.length; activated++){
          activeElements[activated].classList.toggle("active");
     }
})


// //THIS METHOD IS BASED ON STARTING WITH NO LIST ITEMS

// var enterButton = document.getElementById("enter");
// var input = document.getElementById("userInput");
// var ul = document.querySelector("ul");
// var item = document.getElementsByTagName("li");

// function inputLength(){
// 	return input.value.length;
// } 

// function listLength(){
// 	return item.length;
// }

// function createListElement() {
// 	var li = document.createElement("li"); // creates an element "li"
// 	li.appendChild(document.createTextNode(input.value)); //makes text from input field the li text
// 	ul.appendChild(li); //adds li to ul
// 	input.value = ""; //Reset text input field


// 	//START STRIKETHROUGH
// 	// because it's in the function, it only adds it for new items
// 	function crossOut() {
// 		li.classList.toggle("done");
// 	}

// 	li.addEventListener("click",crossOut);
// 	//END STRIKETHROUGH


// 	// START ADD DELETE BUTTON
// 	var dBtn = document.createElement("button");
// 	dBtn.appendChild(document.createTextNode("X"));
// 	li.appendChild(dBtn);
// 	dBtn.addEventListener("click", deleteListItem);
// 	// END ADD DELETE BUTTON


// 	//ADD CLASS DELETE (DISPLAY: NONE)
// 	function deleteListItem(){
// 		li.classList.add("delete")
// 	}
// 	//END ADD CLASS DELETE
// }


// function addListAfterClick(){
// 	if (inputLength() > 0) { //makes sure that an empty input field doesn't create a li
// 		createListElement();
// 	}
// }

// function addListAfterKeypress(event) {
// 	if (inputLength() > 0 && event.which ===13) { //this now looks to see if you hit "enter"/"return"
// 		//the 13 is the enter key's keycode, this could also be display by event.keyCode === 13
// 		createListElement();
// 	} 
// }


// enterButton.addEventListener("click",addListAfterClick);

// input.addEventListener("keypress", addListAfterKeypress);


$(document).ready(
    function(){
        $('#button').click(
            function(){
                var toAdd = $('input[name=ListItem]').val();
                 $('ol').append('<li>' + toAdd + '</li>');
            });
       
       $("input[name=ListItem]").keyup(function(event){
          if(event.keyCode == 13){
            $("#button").click();
          }         
      });
      
      $(document).on('dblclick','li', function(){
        $(this).toggleClass('strike').fadeOut('slow');    
      });
      
      $('input').focus(function() {
        $(this).val('');
      });
      
      $('ol').sortable();  
      
    }
);



//another

function addTodoItem() {
    var todoItem = $("#new-todo-item").val();
    $("#todo-list").append("<li><input type='checkbox'" + 
                           " name='todo-item-done'" + 
                           " class='todo-item-done'"+ 
                           " value='" + todoItem + "' /> " + 
                           todoItem +
                           " <button class='todo-item-delete'>"+
                           "Delete</button></li>");
    
   $("#new-todo-item").val("");
  }
  
  function deleteTodoItem(e, item) {
    e.preventDefault();
    $(item).parent().fadeOut('slow', function() { 
      $(item).parent().remove();
    });
  }
  
                             
  function completeTodoItem() {  
    $(this).parent().toggleClass("strike");
  }
  
  
  $(function() {
   
     $("#add-todo-item").on('click', function(e){
       e.preventDefault();
       addTodoItem()
     });
    
  //EVENT DELEGATION
  //#todo-list is the event handler because .todo-item-delete doesn't exist when the document loads, it is generated later by a todo entry
  //https://learn.jquery.com/events/event-delegation/
    $("#todo-list").on('click', '.todo-item-delete', function(e){
      var item = this; 
      deleteTodoItem(e, item)
    })
    
    $(document).on('click', ".todo-item-done", completeTodoItem)
  
  });