package com.example.patricksheen.bmicalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {


    private EditText enteredHeight = null;
    private EditText enteredWeight = null;
    private Button calculateButton = null;
    private TextView finalResult = null;
    private TextView resultMessage = null;
    private ImageView image = null;
    private CheckBox checkBox = null;
    private TextView weightText = null;
    private TextView heightText = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        enteredHeight = (EditText) findViewById(R.id.enteredHeight);
        enteredWeight = (EditText) findViewById(R.id.enteredWeight);
        finalResult = (TextView) findViewById(R.id.finalResult);
        resultMessage = (TextView) findViewById(R.id.resultMessage);
        image = (ImageView) findViewById(R.id.image);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        weightText = (TextView) findViewById(R.id.weight);
        heightText = (TextView) findViewById(R.id.height);


    }


    public void clickedResult(View view) {

        String message = null;
        Double weight = 0.0;
        Double height = 0.0;

        String enteredHeightStr = enteredHeight.getText().toString();
        Double enteredHeightDouble = Double.parseDouble(enteredHeightStr);

        String enteredWeightStr = enteredWeight.getText().toString();
        Double enteredWeightDouble = Double.parseDouble(enteredWeightStr);

        if (!enteredHeightStr.isEmpty()) {
            height = enteredHeightDouble;

        }
        if (!enteredWeightStr.isEmpty()) {
            weight = enteredWeightDouble;
        }

        Double bmi;
        bmi = calculateBMI(height, weight);
        Integer bmiResult;
        bmiResult = (int) bmi.doubleValue();


        if (enteredHeightDouble >= 250 || enteredHeightDouble <= 20 ||
                enteredWeightDouble >= 160 || enteredWeightDouble <= 10) {
            message = "Please enter valid weight or height!";
            resultMessage.setText(message);
            finalResult.setText("Nothing to display.");
        } else {
            String bmiResultStr = "Your BMI is:" + bmiResult;
            String index = BMIIndex(bmi);
            finalResult.setText(bmiResultStr);
            resultMessage.setText(index);
        }
    }

    private double calculateBMI(double height, double weight) {
        return (double) (weight / (height * height)) * 10000;
    }


    private String BMIIndex(double bmi) {
        if (bmi < 16) {
            image.setImageResource(R.drawable.underweight);
            return "You are Severely Underweight";
        } else if (bmi < 18.5) {
            image.setImageResource(R.drawable.underweight);
            return "You are Underweight";
        } else if (bmi < 25) {
            image.setImageResource(R.drawable.normal);
            return "You are Normal";
        } else if (bmi < 30) {
            image.setImageResource(R.drawable.overweight);
            return "You are Overweight";
        } else if (bmi < 40) {
            image.setImageResource(R.drawable.obese);
            return "You are Obese";
        } else if (bmi >= 40) {
            image.setImageResource(R.drawable.obese);
            return "You are Morbidly Obese";
        } else {
            return "Enter your Details";
        }
    }


    private double imperialBMI(double height, double weight) {
        return (double) ((weight * 0.45) / ((height * 0.0254) * (height * 0.0254)) * 10000);
    }


//
//    public void CheckBox(View view){
//        if(checkBox.isChecked()){
//
//        }else {
//
//
//            enteredWeight.setText("Your weight(pounds): ");
//            enteredHeight.setText("Your height(inch): ");
//            String bmiResultStr = "Your BMI is:" ;
//
//            finalResult.setText(bmiResultStr);
//
//
//
//        }
//    }


    public void Clear(View view) {
        finalResult.setText("");
        enteredHeight.setText("");
        enteredWeight.setText("");
        resultMessage.setText("");
        image.setImageResource(R.drawable.bmi);
        checkBox.isChecked();
    }

}



















